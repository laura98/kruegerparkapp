package cat.itb.krugerparkapp.ui.main;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import cat.itb.krugerparkapp.model.Animal;

import cat.itb.krugerparkapp.repositori.AnimalRepository;


public class MainViewModel extends AndroidViewModel {
    private AnimalRepository animalRepository;
    private LiveData<List<Animal>> animals;
    private boolean isUpdate =false;

    public MainViewModel(@NonNull Application application) {
        super(application);
    }

    public LiveData <List<Animal>> getAnimals(){
        return animalRepository.getAnimals();
    }

}
