package cat.itb.krugerparkapp.ui.main;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import cat.itb.krugerparkapp.R;
import cat.itb.krugerparkapp.adapter.AnimalAdapter;
import cat.itb.krugerparkapp.model.Animal;

public class ListFragment extends Fragment {

    private MainViewModel mViewModel;
    AnimalAdapter adapter;


    RecyclerView recyclerView;

    public static ListFragment newInstance() {
        return new ListFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated( view, savedInstanceState );

    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this.getActivity());
        recyclerView.setLayoutManager(layoutManager);

        adapter = new AnimalAdapter();
        recyclerView.setAdapter(adapter);

        LiveData<List<Animal>> animals = mViewModel.getAnimals();
        animals.observe(this, this::onAnimalChanged);

    }

    private void onAnimalChanged(List<Animal> animals) {
        //??
        adapter.setAnimalList(animals);
    }

}
