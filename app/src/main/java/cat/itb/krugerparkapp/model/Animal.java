package cat.itb.krugerparkapp.model;

import androidx.room.Entity;

@Entity(tableName = "Animals")
public class Animal {


    String animalType;


    public Animal(String animalType) {
        this.animalType = animalType;
    }

    public String getAnimalType() {
        return animalType;
    }

    public void setAnimalType(String animalType) {
        this.animalType = animalType;
    }
}
