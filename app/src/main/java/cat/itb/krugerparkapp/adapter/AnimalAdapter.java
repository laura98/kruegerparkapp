package cat.itb.krugerparkapp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import cat.itb.krugerparkapp.R;
import cat.itb.krugerparkapp.model.Animal;

public class AnimalAdapter extends RecyclerView.Adapter<AnimalAdapter.ListViewHolder> {
    private List<Animal> animalList = new ArrayList<>();
    OnAnimalClickListener listener;

    public AnimalAdapter() {
    }

    public void setAnimalList (List<Animal> animalList){
        this.animalList = animalList;
        notifyDataSetChanged();
    }

    public AnimalAdapter(List<Animal> animalList) {
        this.animalList = animalList;
    }

    @NonNull
    @Override
    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_row,parent,false);
        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListViewHolder holder, int position) {
        Animal animal = animalList.get( position );
        //ESTO HAY QUE CREARLO holder.animalName.setText(animal.getName());
        //holder.idAnimal.setText(animal.getNumberID()+"");
    }

    @Override
    public int getItemCount() {
        int size = 0;
        if (animalList!=null) size= animalList.size();
        return size;
    }

    public class ListViewHolder extends RecyclerView.ViewHolder {
        TextView animalName;


        public ListViewHolder(@NonNull View itemView) {
            super( itemView );
            animalName = itemView.findViewById( R.id.animalNameRow);
        }

        public void onAnimalViewClicked(){
            Animal animal = animalList.get(getAdapterPosition());
            if (listener!=null)
                listener.onAnimalViewClicked(animal);
        }

    }
    public interface OnAnimalClickListener {
        void onAnimalViewClicked (Animal animal);
    }
}