package cat.itb.krugerparkapp.repositori;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;


import cat.itb.krugerparkapp.model.Animal;

public class AnimalRepository {
    private LiveData<List<Animal>> animals;

    public AnimalRepository(Context context){
        
        /*
         Objecte per accedir a firebase/ firestore
         */
    }

    public LiveData<List<Animal>> getAnimal(){return animals;}

    public LiveData<List<Animal>> getAnimals() {
        //accés a dades firestore
        return animals;
    }
}
