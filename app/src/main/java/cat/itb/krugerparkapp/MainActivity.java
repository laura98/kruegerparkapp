package cat.itb.krugerparkapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import cat.itb.krugerparkapp.ui.main.MainFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
    }
}
